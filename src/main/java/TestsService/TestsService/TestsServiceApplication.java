package TestsService.TestsService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestsServiceApplication.class, args);
	}

}
