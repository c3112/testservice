package TestsService.TestsService.controllers;

import TestsService.TestsService.models.Test;
import TestsService.TestsService.repositories.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import util.OnCreateTestEvent;

import java.util.List;

@RestController
@RequestMapping("/api/tests")
public class TestController {

    @Autowired
    TestRepository testRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @GetMapping
    @RequestMapping()
    public List<Test> list() {
        return testRepository.findAll();
    }

    @GetMapping
    @RequestMapping("/{id}")
    public Test get(@PathVariable Long id) {
        if(testRepository.findById(id).isEmpty()) {
            return null;
        }
        return testRepository.getById(id);
    }
    @GetMapping("{username}")
    public List<Test> get(@PathVariable String username) {
        if (testRepository.findAllByUsername(username).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Test with ID " + username + " not found");
        }
        return testRepository.findAllByUsername(username);
    }

    @PostMapping("/doTest")
    @ResponseStatus(HttpStatus.CREATED)
    public Test create(@RequestBody Test test, BindingResult result) {
        testRepository.saveAndFlush(test);
        eventPublisher.publishEvent(new OnCreateTestEvent("/", test));
        if (test.isResult()) {
            kafkaTemplate.send("topic-test", test.getJson());
        }
        return test;
    }
}