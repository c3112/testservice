package TestsService.TestsService.repositories;

import TestsService.TestsService.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TestRepository extends JpaRepository<Test,Long> {
    public Test findByUsername(String username);

    public List<Test> findAllByUsername(String username);
}
