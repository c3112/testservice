package TestsService.TestsService.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.Gson;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity(name="test")
@Access(AccessType.FIELD)
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Test {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long test_id;
    private String username;
    //@DateTimeFormat(pattern = "MM-dd-yyyy")
    private Date test_date;
    private boolean result;

    public Long getTest_id() {
        return test_id;
    }

    public void setTest_id(Long test_id) {
        this.test_id = test_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTest_date() {
        return test_date;
    }

    public void setTest_date(Date test_date) {
        this.test_date = test_date;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getJson(){
        Map test = new HashMap();
        test.put("username",this.username);
        test.put("test_date",this.test_date);
        Gson gson = new Gson();
        String json = gson.toJson(test);
        return json;
    }

    @Override
    public String toString() {
        return "Test{" +
                "test_id=" + test_id +
                ", username='" + username + '\'' +
                ", test_date=" + test_date +
                ", result=" + result +
                '}';
    }
}
