package util;

import TestsService.TestsService.models.Test;
import org.springframework.context.ApplicationEvent;

public class OnCreateTestEvent extends ApplicationEvent {
    private String appUrl;
    private Test test;
    public OnCreateTestEvent(String appUrl, Test test) {
        super(test);
        this.appUrl = appUrl;
        this.test = test;
    }
    public String getAppUrl() {
        return appUrl;
    }
    public Test getTest() {
        return test;
    }
}
